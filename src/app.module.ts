import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsModule } from './news/news.module';
import { MongooseModule } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios';
import { NewsController } from './news/news.controller';
import { NewsService } from './news/news.service';
import { News, NewsSchema } from './news/schema/news.schema';

@Module({
  imports: [
    HttpModule,
    NewsModule,
    MongooseModule.forRoot(
      'mongodb+srv://admin:1234@cluster0.eco8iji.mongodb.net/?retryWrites=true&w=majority',
    ),
    MongooseModule.forFeature([{ name: News.name, schema: NewsSchema }]),
  ],
  controllers: [AppController, NewsController],
  providers: [AppService, NewsService],
})
export class AppModule {}
