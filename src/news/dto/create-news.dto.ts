/* eslint-disable prettier/prettier */

export class CreateNewsDto {

  readonly objectID: number;

  readonly story_title: string;

  readonly title: string;

  readonly author: string;

  readonly created_at: Date;

  readonly story_url: string;

  readonly url: string;

}
