/* eslint-disable prettier/prettier */
import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose'
import { Document } from 'mongoose';

export type NewsSchema = News & Document;

@Schema()
export class News {
  @Prop({required:true})
  objectID: number;

  @Prop({required:true})
  story_title: string;

  @Prop({required:true})
  title: string;

  @Prop({required:true})
  author: string;

  @Prop({required:true})
  created_at: Date;

  @Prop({required:true})
  story_url: string;

  @Prop({required:true})
  url: string;

}

export const NewsSchema = SchemaFactory.createForClass(News);
