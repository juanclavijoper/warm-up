/* eslint-disable prettier/prettier */
import { Controller, Get, Delete, Body, Post } from '@nestjs/common';
// import { CreateNewsDto } from './dto/create-news.dto';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {

    constructor(private newsService: NewsService){
    }

    @Get()
    getNews(){
        return this.newsService.getNews();
    }
}



/*
    @Post()
    async create(@Body() createNewsDto: CreateNewsDto) {
        return this.newService.create(createNewsDto);
    }
    
    @Get(':id')
    getSingleNews(@Param('id') id){
        return this.newService.getSingleNews(id);
    }
  

    @Delete(':id')
    deleteNews(@Param('id') id){
        return this.newsService.deleteNews(id);
    }*/
