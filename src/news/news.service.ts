import { HttpService } from '@nestjs/axios';

import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { map } from 'rxjs/operators';
import { CreateNewsDto } from './dto/create-news.dto';
import { News } from './schema/news.schema';

@Injectable()
export class NewsService {
  constructor(
    @InjectModel(News.name) private newsModule: Model<Document>,
    private http: HttpService,
  ) {}

  create(createNewsDto: CreateNewsDto) {
    this.newsModule.create(createNewsDto);
  }

  getNews() {
    return this.http
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(map((response) => response.data));
  }

  /*
  getSingleNews(_id) {
    return this.http
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs' + _id)
      .pipe(map((response) => response.data));
  }

  postNews(_id) {
    return this.http
      .post('https://hn.algolia.com/api/v1/search_by_date?query=nodejs' + _id)
      .pipe(map((response) => response.data.Schema));
  }*/
}
