FROM node:18-alpine as dev

ARG PORT=3333

WORKDIR /app
COPY . .

RUN npm install

EXPOSE $PORT

CMD [ "npm", "run", "start:dev" ]